<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/videotool', function () {
    return view('abc');
});
Route::get('media/cache/resolve/squared_thumbnail/userfiles/projects/{project}/{image}','ProjectController@image');
