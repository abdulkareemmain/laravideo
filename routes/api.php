<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/project_progress/{id}','ProjectController@progressAction');
Route::post('/media','ProjectController@index');
Route::get('/project/{id}','ProjectController@edit');
Route::post('/project/{id}','ProjectController@updateAction');
Route::post('/project/{uniqueId}/upload','ProjectController@uploadFileAction');

Route::get('/library_music','ProjectController@libraryMusicAction');




