<?php

namespace App\Repository;

use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    /**
     * @param int $limit
     * @return Project[]
     */
    public function findPending($limit = null)
    {
        return $this->findBy([
            'status' => Project::STATUS_PENDING
        ], [
            'createdAt' => 'asc'
        ], $limit);
    }

    /**
     * @param int $limit
     * @return Project[]
     */
    public function findProcessing($limit = null)
    {
        return $this->findBy([
            'status' => Project::STATUS_PROCESSING
        ], [
            'createdAt' => 'asc'
        ], $limit);
    }

    /**
     * @param $statusName
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCountByStatus($statusName)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->where('p.status = :statusName')
            ->setParameter('statusName', $statusName);

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param int $hours
     * @return mixed
     * @throws \Exception
     */
    public function findOld($hours = 3)
    {
        $date = new \Datetime('now');
        $date->modify("-{$hours} hour");

        $qb = $this->createQueryBuilder('p');
        $qb
            ->andWhere('p.isPublic = 0')
            ->andWhere('p.status NOT IN(:statusesNot)')
            ->andWhere('p.updatedAt < :dateTime')
            ->setParameters([
                'dateTime' => $date,
                'statusesNot' => [Project::STATUS_PENDING, Project::STATUS_PROCESSING]
            ]);

        return $qb->getQuery()->getResult();
    }

    public function getCount()
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('count(p.id)');
        return $qb->getQuery()->getSingleScalarResult();
    }
}
