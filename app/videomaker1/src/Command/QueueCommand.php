<?php

namespace App\Command;

use Andchir\VideoProcessing;
use Andchir\VideoSlideShow;
use App\Entity\Media;
use App\Entity\Project;
use App\Repository\MediaRepository;
use App\Repository\ProjectRepository;
use Symfony\Bridge\Doctrine\Form\ChoiceList\EntityLoaderInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class QueueCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:queue';

    protected function configure()
    {
        $this
            ->setDescription('Queue for movie rendering');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $em = $this->getContainer()->get('doctrine')->getManager();

        /** @var ProjectRepository $projectRepository */
        $projectRepository = $em->getRepository(Project::class);

        $queueSize = (int) $this->getContainer()->getParameter('app.queue_size');
        $projectsProcessing = $projectRepository->findProcessing();
        $countProcessing = count($projectsProcessing);

        // Process rendered projects
        $completedCount = $this->processProjectsProcessing();

        $io->note(sprintf("Was processing: {$countProcessing}, Completed: {$completedCount}"));

        $countProcessing -= $completedCount;
        $countAvailable = $queueSize - $countProcessing;
        $countPendingStarted = 0;
        $countPending = $projectRepository->getCountByStatus(Project::STATUS_PENDING);
        // Process pending projects
        if ($countAvailable > 0) {
            $countPendingStarted = $this->processProjectsPending($countAvailable);
        } else {
            $io->note(sprintf("Now pending: {$countPending}, Available for processing: {$countAvailable}"));
        }

        $io->success("Queue limit: {$queueSize}, New started: {$countPendingStarted}");
    }

    /**
     * @return int
     */
    public function processProjectsProcessing()
    {
        $count = 0;
        /** @var EntityLoaderInterface $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var ProjectRepository $projectRepository */
        $projectRepository = $em->getRepository(Project::class);

        $uploadDirPath = $this->getUploadDirPath();
        $videoProcessing = new VideoProcessing([
            'melt_path' => $this->getContainer()->getParameter('app.melt_path'),
            'tmp_dir_path' => realpath($this->getContainer()->getParameter('app.temp_dir_path')),
            'session_start' => false,
            'logging' => $this->getContainer()->getParameter('app.debug')
        ]);

        $projectsProcessing = $projectRepository->findProcessing();
        /** @var Project $project */
        foreach ($projectsProcessing as $project) {
            $pid = $project->getOptionValue('pid');
            if (!$videoProcessing->isRunning($pid)) {

                $outputFilePath = $project->getOptionValue('outputPath');
                if (!file_exists($outputFilePath)) {
                    $project->setStatus(Project::STATUS_ERROR);
                    $em->flush();
                    continue;
                }

                $oldMedia = $project->getMovie();
                if ($oldMedia && $oldMedia instanceof Media) {
                    $em->remove($oldMedia);
                }

                $media = new Media();
                $media
                    ->setTitle('Slide show movie')
                    ->setProjectId($project->getId())
                    ->setType(Media::TYPE_VIDEO)
                    ->setSize(filesize($outputFilePath))
                    ->setFileName(basename($outputFilePath));

                $project
                    ->setMovie($media)
                    ->setStatus(Project::STATUS_COMPLETED)
                    ->setOptionValue('pid', 0)
                    ->setOptionValue('progressLogPath', '')
                    ->setOptionValue('outputPath', '');
                $em->flush();

                $count++;
            }
        }

        return $count;
    }

    /**
     * @param $limit
     * @return int
     * @throws \ImagickException
     */
    public function processProjectsPending($limit)
    {
        /** @var EntityLoaderInterface $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var ProjectRepository $projectRepository */
        $projectRepository = $em->getRepository(Project::class);

        $uploadDirPath = $this->getUploadDirPath();
        $projectsPending = $projectRepository->findPending($limit);
        $count = 0;

        /** @var Project $project */
        foreach ($projectsPending as $project) {
            $projectDirPath = $uploadDirPath . DIRECTORY_SEPARATOR . $project->getUniqueId();
            $outputFilePath = $projectDirPath . DIRECTORY_SEPARATOR . Media::generateUniqueFileName() . '.mp4';
            $result = $this->startRendering($project, $outputFilePath);
            if ($result !== false) {
                list($pid, $progressLogPath) = $result;
                if ($pid && $progressLogPath) {
                    $project
                        ->setOptionValue('pid', $pid)
                        ->setOptionValue('progressLogPath', $progressLogPath)
                        ->setOptionValue('outputPath', $outputFilePath)
                        ->setStatus(Project::STATUS_PROCESSING);
                    $count++;
                }
            } else {
                $project->setStatus(Project::STATUS_CANCELED);
            }
            $em->flush();
        }
        return $count;
    }

    /**
     * @param Project $project
     * @param $outputFilePath
     * @return array|bool
     * @throws \ImagickException
     */
    public function startRendering(Project $project, $outputFilePath)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var MediaRepository $mediaRepository */
        $mediaRepository = $em->getRepository(Media::class);
        $uploadDirPath = $this->getUploadDirPath();

        $videoSlideShow = new VideoSlideShow();
        $videoProcessing = new VideoProcessing([
            'melt_path' => $this->getContainer()->getParameter('app.melt_path'),
            'tmp_dir_path' => realpath($this->getContainer()->getParameter('app.temp_dir_path')),
            'wipes_dir_path' => $this->getRootPath() . '/public/assets/wipes',
            'session_start' => false,
            'logging' => $this->getContainer()->getParameter('app.debug')
        ]);
        $watermarkImagePath = $this->getContainer()->hasParameter('app.watermark_image_path')
            ? realpath($this->getContainer()->getParameter('app.watermark_image_path'))
            : '';
        $durationClipDefault = 300;
        $durationTransaction = 50;

        $mediaList = $mediaRepository->findProjectMedia($project->getId(), true);
        if (empty($mediaList)) {
            return false;
        }
        $projectDirPath = $uploadDirPath . DIRECTORY_SEPARATOR . $project->getUniqueId();

        $profileName = $project->getOptionValue('size') === 'full_hd' ? 'atsc_1080p_25' : 'atsc_720p_25';
        $musicFilePath = $this->getMusicAudioFilePath($project);
        $imagesCount = 0;
        $durationTotal = 0;

        $videoProcessing
            ->setProfile($profileName)
            ->addOption(['inputSource' => [
                'colour:black', ['out' => $durationTransaction]
            ]]);

        /** @var Media $media */
        foreach ($mediaList as $index => $media) {
            if ($media->getType() !== Media::TYPE_IMAGE) {
                continue;
            }
            $media->setFileTargetDir($projectDirPath);
            $imagePath = $media->getFilePath();
            if (!file_exists($imagePath)) {
                continue;
            }

            $this->autoRotateImage($imagePath);

            list($imageWidth, $imageHeight) = getimagesize($imagePath);

            $mediaMotionOptions = $media->getOptionValue('customized')
                ? $media->getMotionOptions()
                : [];

            $durationClip = (int) $media->getOptionValue('duration', $durationClipDefault);
            
            $transformGeometry = $videoSlideShow->getPositionFilterOptionValue(
                !empty($mediaMotionOptions) ? 'custom' : 'auto',
                $durationClip,
                $imageWidth,
                $imageHeight,
                $videoProcessing->getWidth(),
                $videoProcessing->getHeight(),
                $imagePath,
                $mediaMotionOptions
            );

            $videoProcessing
                ->addOption(['inputSource' => [
                    $imagePath, ['out' => $durationClip]
                ]])
                // Add filter zoom and position
                ->addOption(['attach' => [
                    'affine',
                    [
                        'background' => 'colour:0x00000000',
                        'transition.align' => 'left',
                        'transition.valign' => 'top',
                        'transition.distort' => '0',
                        'transition.geometry' => $transformGeometry
                    ]
                ]]);

            $slideText = $media->getTextTitle();
            if ($slideText) {
                $textPosition = $media->getOptionValue('text_position', 'top_left');
                $textPos = explode('_', $textPosition);
                $slideText = str_replace('#', '\#', $slideText);
                $videoProcessing
                    ->addTextOverlay($slideText, false, [
                        'pad' => '50x0',
                        'size' => 60 * ($videoProcessing->getHeight() / 720),
                        'halign' => $textPos[1],
                        'valign' => $textPos[0],
                        'family' => 'Ubuntu',
                        'weight' => 700,
                        'slideFrom' => 'left',
                        'in' => 50,
                        'duration' => 50
                    ]);
            }

            $transitionName = $media->getOptionValue('transition', 'fade');
            if (strpos($transitionName, 'wipe__') !== false) {
                $wipeOptions = explode('__', $transitionName);
                $videoProcessing->addReadyMadeTransition('wipeIn', $durationTransaction, [
                    'wipeName' => $wipeOptions[1] . '.pgm'
                ]);
            } else {
                $videoProcessing->addReadyMadeTransition($transitionName, $durationTransaction);
            }
            $durationTotal += $durationClip;
            $imagesCount++;
        }
        if ($imagesCount === 0) {

            return false;
        }

        $durationTotal = ($durationTotal - (($durationTransaction - 1) * ($imagesCount - 1)));

        $videoProcessing
            // Add black
            ->addOption(['inputSource' => [
                'colour:black', ['out' => $durationTransaction]
            ]])
            ->addReadyMadeTransition('fade', $durationTransaction)
            // Add background audio
            ->addBackgroundAudio($musicFilePath, ['out' => $durationTotal])
            ->addOption(['filter' => [
                'volume',
                ['gain' => 1, 'end' => 0],
                ['in' => $durationTotal - ($durationTransaction - 1), 'out' => $durationTotal]
            ]])->setOutputVideoOptions($outputFilePath);

        if ($watermarkImagePath && file_exists($watermarkImagePath)) {
            $watermarkPosition = $this->getWatermarkPosition($project->getOptionValue('size'));
            $videoProcessing
                ->addWatermark($watermarkImagePath, false, $watermarkPosition);
        }

        // echo $videoProcessing->getCommandOutput(); exit;

        return $videoProcessing->render();
    }

    /**
     * @param string $sizeValue
     * @return array
     */
    public function getWatermarkPosition($sizeValue = 'hd')
    {
        $size = $sizeValue == 'full_hd' ? [1920, 1080] : [1280, 720];
        $scale = $size[1] / 720;
        if ($this->getContainer()->hasParameter('app.watermark_position')) {
            $watermarkPosition = $this->getContainer()->getParameter('app.watermark_position');
        } else {
            $watermarkPosition = ['width' => 300, 'height' => 300, 'left' => 20, 'top' => 20];
        }
        $watermarkPosition['width'] = round($watermarkPosition['width'] * $scale);
        $watermarkPosition['height'] = round($watermarkPosition['height'] * $scale);
        if ($watermarkPosition['left'] < 0) {
            $watermarkPosition['left'] = $size[0] - $watermarkPosition['width'] + $watermarkPosition['left'];
        }
        if ($watermarkPosition['top'] < 0) {
            $watermarkPosition['top'] = $size[1] - $watermarkPosition['height'] + $watermarkPosition['top'];
        }

        return $watermarkPosition;
    }

    /**
     * @param $imagePath
     * @throws \ImagickException
     */
    public function autoRotateImage($imagePath) {
        $image = new \Imagick($imagePath);
        $orientation = $image->getImageOrientation();

        switch($orientation) {
            case \Imagick::ORIENTATION_BOTTOMRIGHT:
                $image->rotateimage("#000", 180); // rotate 180 degrees
                break;

            case \Imagick::ORIENTATION_RIGHTTOP:
                $image->rotateimage("#000", 90); // rotate 90 degrees CW
                break;

            case \Imagick::ORIENTATION_LEFTBOTTOM:
                $image->rotateimage("#000", -90); // rotate 90 degrees CCW
                break;
        }
        if ($orientation !== \Imagick::ORIENTATION_TOPLEFT) {
            $image->setImageOrientation(\Imagick::ORIENTATION_TOPLEFT);
            $image->writeImage($imagePath);
        }
    }

    /**
     * @param Project $project
     * @return string
     */
    public function getMusicAudioFilePath(Project $project)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var MediaRepository $mediaRepository */
        $mediaRepository = $em->getRepository(Media::class);

        $output = '';
        $musicName = $project->getOptionValue('musicName');
        $musicId = $project->getOptionValue('musicId');
        $uploadDirPath = $this->getUploadDirPath();
        $projectDirPath = $uploadDirPath . DIRECTORY_SEPARATOR . $project->getUniqueId();
        if ($musicName === 'Uploaded' && $musicId) {
            $musicMedia = $mediaRepository->findOneBy([
                'projectId' => $project->getId(),
                'type' => Media::TYPE_AUDIO,
                'id' => $musicId
            ]);
            if ($musicMedia) {
                $musicMedia->setFileTargetDir($projectDirPath);
                $output = $musicMedia->getFilePath();
            }
        }
        else if ($musicName) {

            $libraryDirPath = $this->getContainer()->getParameter('app.music_library_dir_path');
            $output = $libraryDirPath . DIRECTORY_SEPARATOR . $musicName;
        }

        return $output && file_exists($output) ? realpath($output) : '';
    }

    /**
     * @return bool|string
     */
    public function getRootPath()
    {
        $rootPath = realpath($this->getContainer()->getParameter('kernel.root_dir') . '/../');
        return $rootPath;
    }

    /**
     * @return string
     */
    public function getUploadDirPath()
    {
        return realpath($this->getContainer()->getParameter('app.project_dir_path'));
    }
}
