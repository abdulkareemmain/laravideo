<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* nav/pagination_simple.html.twig */
class __TwigTemplate_489e6341c4f7321a8c222228cda7203262f5a42a9b5dd5a67028c8f554b49621 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "nav/pagination_simple.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "nav/pagination_simple.html.twig"));

        // line 1
        if ((twig_get_attribute($this->env, $this->source, (isset($context["pagesOptions"]) || array_key_exists("pagesOptions", $context) ? $context["pagesOptions"] : (function () { throw new RuntimeError('Variable "pagesOptions" does not exist.', 1, $this->source); })()), "total", [], "any", false, false, false, 1) > 1)) {
            // line 2
            echo "    <nav>
        <ul class=\"pagination pagination-rounded\">
            ";
            // line 4
            if ((twig_get_attribute($this->env, $this->source, (isset($context["pagesOptions"]) || array_key_exists("pagesOptions", $context) ? $context["pagesOptions"] : (function () { throw new RuntimeError('Variable "pagesOptions" does not exist.', 4, $this->source); })()), "current", [], "any", false, false, false, 4) != twig_get_attribute($this->env, $this->source, (isset($context["pagesOptions"]) || array_key_exists("pagesOptions", $context) ? $context["pagesOptions"] : (function () { throw new RuntimeError('Variable "pagesOptions" does not exist.', 4, $this->source); })()), "prev", [], "any", false, false, false, 4))) {
                // line 5
                echo "                <li class=\"page-item\">
                    <a class=\"page-link\" href=\"";
                // line 6
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 6, $this->source); })()), "request", [], "any", false, false, false, 6), "attributes", [], "any", false, false, false, 6), "get", [0 => "_route"], "method", false, false, false, 6), ["page" => twig_get_attribute($this->env, $this->source, (isset($context["pagesOptions"]) || array_key_exists("pagesOptions", $context) ? $context["pagesOptions"] : (function () { throw new RuntimeError('Variable "pagesOptions" does not exist.', 6, $this->source); })()), "prev", [], "any", false, false, false, 6)]), "html", null, true);
                echo "\" aria-label=\"Previous\">
                        <span aria-hidden=\"true\">&laquo;</span>
                        <span class=\"sr-only\">Previous</span>
                    </a>
                </li>
            ";
            }
            // line 12
            echo "
            ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->source, (isset($context["pagesOptions"]) || array_key_exists("pagesOptions", $context) ? $context["pagesOptions"] : (function () { throw new RuntimeError('Variable "pagesOptions" does not exist.', 13, $this->source); })()), "total", [], "any", false, false, false, 13)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 14
                echo "                <li class=\"page-item";
                if (($context["i"] == twig_get_attribute($this->env, $this->source, (isset($context["pagesOptions"]) || array_key_exists("pagesOptions", $context) ? $context["pagesOptions"] : (function () { throw new RuntimeError('Variable "pagesOptions" does not exist.', 14, $this->source); })()), "current", [], "any", false, false, false, 14))) {
                    echo " active";
                }
                echo "\">
                    <a class=\"page-link\" href=\"";
                // line 15
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 15, $this->source); })()), "request", [], "any", false, false, false, 15), "attributes", [], "any", false, false, false, 15), "get", [0 => "_route"], "method", false, false, false, 15), ["page" => $context["i"]]), "html", null, true);
                echo "\">
                        ";
                // line 16
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "
                    </a>
                </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "
            ";
            // line 21
            if ((twig_get_attribute($this->env, $this->source, (isset($context["pagesOptions"]) || array_key_exists("pagesOptions", $context) ? $context["pagesOptions"] : (function () { throw new RuntimeError('Variable "pagesOptions" does not exist.', 21, $this->source); })()), "current", [], "any", false, false, false, 21) != twig_get_attribute($this->env, $this->source, (isset($context["pagesOptions"]) || array_key_exists("pagesOptions", $context) ? $context["pagesOptions"] : (function () { throw new RuntimeError('Variable "pagesOptions" does not exist.', 21, $this->source); })()), "next", [], "any", false, false, false, 21))) {
                // line 22
                echo "                <li class=\"page-item\">
                    <a class=\"page-link\" href=\"";
                // line 23
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 23, $this->source); })()), "request", [], "any", false, false, false, 23), "attributes", [], "any", false, false, false, 23), "get", [0 => "_route"], "method", false, false, false, 23), ["page" => twig_get_attribute($this->env, $this->source, (isset($context["pagesOptions"]) || array_key_exists("pagesOptions", $context) ? $context["pagesOptions"] : (function () { throw new RuntimeError('Variable "pagesOptions" does not exist.', 23, $this->source); })()), "next", [], "any", false, false, false, 23)]), "html", null, true);
                echo "\" aria-label=\"Next\">
                        <span aria-hidden=\"true\">&raquo;</span>
                        <span class=\"sr-only\">Next</span>
                    </a>
                </li>
            ";
            }
            // line 29
            echo "        </ul>
    </nav>
";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "nav/pagination_simple.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 29,  99 => 23,  96 => 22,  94 => 21,  91 => 20,  81 => 16,  77 => 15,  70 => 14,  66 => 13,  63 => 12,  54 => 6,  51 => 5,  49 => 4,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if pagesOptions.total > 1 %}
    <nav>
        <ul class=\"pagination pagination-rounded\">
            {% if pagesOptions.current != pagesOptions.prev %}
                <li class=\"page-item\">
                    <a class=\"page-link\" href=\"{{ path(app.request.attributes.get('_route'), {'page': pagesOptions.prev}) }}\" aria-label=\"Previous\">
                        <span aria-hidden=\"true\">&laquo;</span>
                        <span class=\"sr-only\">Previous</span>
                    </a>
                </li>
            {% endif %}

            {% for i in 1..pagesOptions.total %}
                <li class=\"page-item{% if i == pagesOptions.current %} active{% endif %}\">
                    <a class=\"page-link\" href=\"{{ path(app.request.attributes.get('_route'), {'page': i}) }}\">
                        {{ i }}
                    </a>
                </li>
            {% endfor %}

            {% if pagesOptions.current != pagesOptions.next %}
                <li class=\"page-item\">
                    <a class=\"page-link\" href=\"{{ path(app.request.attributes.get('_route'), {'page': pagesOptions.next}) }}\" aria-label=\"Next\">
                        <span aria-hidden=\"true\">&raquo;</span>
                        <span class=\"sr-only\">Next</span>
                    </a>
                </li>
            {% endif %}
        </ul>
    </nav>
{% endif %}
", "nav/pagination_simple.html.twig", "C:\\wamp64\\www\\videomaker\\templates\\nav\\pagination_simple.html.twig");
    }
}
