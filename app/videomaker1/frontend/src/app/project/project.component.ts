import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {DragulaService} from 'ng2-dragula';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {TranslateService} from '@ngx-translate/core';

import {ProjectService} from '../services/project.service';
import {Project} from '../models/project.model';
import {Media} from '../models/media.model';
import {ModalPhotoEditComponent} from './modal-edit.component';

@Component({
    selector: 'app-project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit, OnDestroy {

    @ViewChild('progressBar') progressBar;
    @ViewChild('fileInput') fileInput;
    loading = false;
    renderingStarted = false;
    loadingAudio = false;
    errorMessage = '';
    project = new Project('', '');
    items: Media[] = [];
    progressMessage = '';
    music: string[] = [];
    audioPlayer = new Audio();
    musicUplodedFileUrl = '';
    movieFileUrl = '';
    timer: any;
    bsModalRef: BsModalRef;

    constructor(
        private router: Router,
        private dataService: ProjectService,
        private route: ActivatedRoute,
        private dragulaService: DragulaService,
        private modalService: BsModalService,
        public translateService: TranslateService
    ) {
        this.dragulaService.destroy('mediaItems');
        dragulaService.createGroup('mediaItems', {
            moves: (el, container, handle) => {
                return handle.className.indexOf('drag-handle') > -1;
            }
        });
        this.project.options.size = 'hd';
        this.project.options.musicName = '';
        this.project.options.musicId = 0;
    }

    ngOnInit() {
        this.getProject();
        this.getMusic();
    }

    getLangString(value: string): string {
        if (!this.translateService.store.translations[this.translateService.currentLang]) {
            return value;
        }
        const translations = this.translateService.store.translations[this.translateService.currentLang];
        return translations[value] || value;
    }

    getProject(): void {
        this.project.uniqueId = this.route.snapshot.paramMap.get('uniqueId');
        this.loading = true;
        this.errorMessage = '';
        this.dataService.getItem(this.project.uniqueId)
            .subscribe((res) => {
                this.project.status = res['status'];
                if (res['items']) {
                    this.items = res['items'] as Media[];
                }
                // if (res['options'] && !Array.isArray(res['options'])) {
                //     this.project.options = res['options'];
                // }
                if (res['options'] && res['options']['musicId']) {
                    this.project.options.musicId = res['options']['musicId'];
                }
                if (res['options'] && res['options']['musicName']) {
                    this.project.options.musicName = res['options']['musicName'];
                }
                if (res['musicFileUrl']) {
                    this.musicUplodedFileUrl = res['musicFileUrl'];
                    this.music.push('Uploaded');
                    this.project.options.musicName = 'Uploaded';
                }
                if (res['movieUrl']) {
                    this.movieFileUrl = res['movieUrl'];
                }
                if (['pending', 'processing'].indexOf(this.project.status) > -1) {
                    this.getProgress();
                }
                this.loading = false;
                this.onGetProject(res);
            }, (err) => {
                if (err['error']) {
                    this.errorMessage = err['error'];
                }
                this.loading = false;
            });
    }

    onGetProject(responce: any) {
        if (window.parent && typeof window.parent['vssmExportUrl'] === 'function') {
            window.parent['vssmExportUrl'](responce);
        }
    }

    getMusic(): void {
        this.dataService.getMusic()
            .subscribe((res) => {
                this.music = res as string[];
                if (!this.project.options.musicName) {
                    this.project.options.musicName = this.music[0];
                }
            }, (err) => {
                if (err['error']) {
                    this.errorMessage = err['error'];
                }
                this.loading = false;
            });
    }

    openModalPhotoEditComponent(media: Media, event?: MouseEvent) {
        if (event) {
            event.preventDefault();
        }
        const initialState = {
            media: media,
            project: this.project
        };
        this.bsModalRef = this.modalService.show(ModalPhotoEditComponent, {
            initialState,
            animated: false,
            ignoreBackdropClick: true,
            'class': 'modal-lg'
        });
    }

    getImageUrl(media: Media): string {
        return Media.getMediaResizedUrl(media, this.project.uniqueId);
    }

    getProgress(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
            this.dataService.getProgress(this.project.uniqueId)
                .subscribe((res) => {
                    if (res['queue_number']) {
                        this.progressMessage = `${this.getLangString('YOU_QUEUE_NUMBER')}: ${res['queue_number']}`;
                    }
                    let percent = 0;
                    if (res['percent']) {
                        percent = res['percent'];
                        this.progressBar.nativeElement.style.width = `${percent}%`;
                        this.progressMessage = `${percent}%`;
                    }
                    if (percent === 100) {
                        setTimeout(this.getProject.bind(this), 2000);
                    } else {
                        this.getProgress();
                    }
                },
                (err) => {
                    if (err['error']) {
                        this.errorMessage = err['error'];
                    }
                });
        }, 3000);
    }

    playAudio(event?: MouseEvent): void {
        if (event) {
            event.preventDefault();
        }
        if (!this.project.options.musicName) {
            return;
        }
        const audioUrl = this.project.options.musicName === 'Uploaded'
            ? this.musicUplodedFileUrl
            : `library/music/${this.project.options.musicName}`;
        if (!this.audioPlayer.paused && decodeURIComponent(this.audioPlayer.src).indexOf(audioUrl) > -1) {
            this.audioPlayer.pause();
            return;
        }
        this.audioPlayer.src = audioUrl;
        this.audioPlayer.play();
    }

    selectFile(event?: MouseEvent): void {
        if (event) {
            event.preventDefault();
        }
        this.fileInput.nativeElement.click();
    }

    onFileSelect(): void {
        const files = this.fileInput.nativeElement.files;
        const data = {
            type: 'music',
            file: files[0]
        };
        const url = this.dataService.getRequestFullUrl('upload', this.project.uniqueId);
        this.errorMessage = '';
        this.loadingAudio = true;
        this.dataService.postFormData(this.dataService.getFormData(data), url)
            .subscribe((res) => {
                if (res['success']) {
                    if (res['musicId']) {
                        this.project.options.musicId = res['musicId'];
                        this.project.options.musicName = 'Uploaded';
                        if (this.music.indexOf('Uploaded') === -1) {
                            this.music.push('Uploaded');
                        }
                        if (res['musicFileUrl']) {
                            this.musicUplodedFileUrl = res['musicFileUrl'];
                        }
                    }
                }
                this.fileInput.nativeElement.files = null;
                this.fileInput.nativeElement.value = null;
                this.loadingAudio = false;
            }, (err) => {
                if (err['error']) {
                    this.errorMessage = err['error'];
                }
                this.loadingAudio = false;
            });

    }

    createProject(): void {
        if (!this.audioPlayer.paused) {
            this.audioPlayer.pause();
        }
        if (this.renderingStarted) {
            return;
        }
        this.errorMessage = '';
        this.renderingStarted = true;
        this.dataService.saveProjectData(this.project.uniqueId, {
            options: this.project.options,
            items: this.items
        })
            .subscribe((res) => {
                    if (res['success']) {
                        this.project.status = 'pending';
                    }
                    this.getProgress();
                },
                (err) => {
                    if (err['error']) {
                        this.errorMessage = err['error'];
                    }
                    this.loading = false;
                    this.renderingStarted = false;
                });
    }

    ngOnDestroy(): void {
        if (!this.audioPlayer.paused) {
            this.audioPlayer.pause();
        }
        this.dragulaService.destroy('mediaItems');
    }

}
