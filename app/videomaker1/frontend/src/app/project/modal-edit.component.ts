import {Component, OnInit, ViewChild} from '@angular/core';
import {BsModalService, BsModalRef} from 'ngx-bootstrap';
import {CropperComponent} from 'angular-cropperjs';

import {Media} from '../models/media.model';
import {Project} from '../models/project.model';

@Component({
    selector: 'app-modal-photo-edit',
    templateUrl: 'modal-photo-edit.html'
})
export class ModalPhotoEditComponent implements OnInit {

    @ViewChild('imageElement') imageElement;
    @ViewChild('angularCropper') angularCropper: CropperComponent;
    media: Media;
    project: Project;
    imageUrl: string;
    cropperOptions: any;
    options: {[key: string]: string|number|boolean};
    motionOptions: {from: {[key: string]: string|number}, to: {[key: string]: string|number}};
    position = 'from';
    isEditMode = false;
    isExtendMode = false;

    constructor(public bsModalRef: BsModalRef) {}

    ngOnInit() {
        this.motionOptions = Object.assign({}, {from: {}, to: {}}, this.media.motionOptions);
        this.options = Object.assign({}, this.media.options);
        if (!this.options.duration) {
            this.options.duration = 300;
        }
        if (!this.options.text_position) {
            this.options.text_position = 'top_left';
        }
        if (!this.options.transition) {
            this.options.transition = 'fade';
        }
        this.isEditMode = this.media.options ? !!this.media.options['customized'] : false;
        this.cropperOptions = {
            aspectRatio: 1280 / 720,
            checkCrossOrigin: false,
            checkOrientation: true,
            autoCrop: false,
            data: this.motionOptions.from
        };
        this.imageUrl = Media.getMediaFullUrl(this.media.fileName, this.project.uniqueId);
    }

    onCropperReady(event?: CustomEvent): void {
        if (event && event.target) {
            this.cropperToggle();
        }
    }

    onModeChange(value: boolean): void {
        this.isEditMode = value;
        this.position = 'from';
        this.cropperToggle();
    }

    cropperToggle(): void {
        if (this.isEditMode && this.angularCropper.cropper) {
            this.angularCropper.cropper.enable();
            this.angularCropper.cropper.crop();
            if (Object.keys(this.motionOptions.from).length > 0) {
                this.angularCropper.cropper.setData(Object.assign({}, this.motionOptions.from));
            }
        } else {
            this.angularCropper.cropper.clear();
            this.angularCropper.cropper.disable();
        }
    }

    onPositionChange(value: string): void {
        this.motionOptions[this.position] = this.angularCropper.cropper.getData(true);
        this.position = value;
        if (this.motionOptions[value]) {
            this.angularCropper.cropper.reset();
            this.angularCropper.cropper.setData(Object.assign({}, this.motionOptions[value]));
        }
    }

    close(event: MouseEvent): void {
        event.preventDefault();
        this.bsModalRef.hide();
    }

    save(event?: MouseEvent): void {
        if (event) {
            event.preventDefault();
        }
        if (!this.media.options || Array.isArray(this.media.options)) {
            this.media.options = {};
        }
        if (!this.media.motionOptions || Array.isArray(this.media.motionOptions)) {
            this.media.motionOptions = {from: {}, to: {}};
        }
        if (this.angularCropper && this.angularCropper.cropper) {
            this.motionOptions[this.position] = this.angularCropper.cropper.getData(true);
        }
        Object.assign(this.media.options, this.options, {customized: this.isEditMode});
        Object.assign(this.media.motionOptions, this.motionOptions);
        this.bsModalRef.hide();
    }
}
