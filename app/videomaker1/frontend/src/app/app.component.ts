import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {TranslateService} from '@ngx-translate/core';
import {catchError, map, tap} from 'rxjs/operators';

declare const Brusher: any;
declare const appSettings: any;
declare const window: Window;

interface LanguageData {
    code: string;
    name: string;
    icon: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    locale = 'en';
    displayLanguageSwitch: boolean;
    displayHomepageVideo: boolean;
    displayHomepageFeatures: boolean;
    languages: LanguageData[] = [];

    constructor(
        private httpClient: HttpClient,
        private translate: TranslateService
    ) {
        this.locale = appSettings.locale;
        this.displayLanguageSwitch = appSettings.displayLanguageSwitch;
        this.displayHomepageVideo = appSettings.displayHomepageVideo;
        this.displayHomepageFeatures = appSettings.displayHomepageFeatures;
        this.languages = appSettings.languages;
        this.translate.addLangs(['en', 'ru']);
        this.translate.setDefaultLang('en');
        this.translate.use(this.locale);
    }

    ngOnInit() {
        const brusher = new Brusher({
            image: 'assets/img/background6.jpg',
            keepCleared: false,
            stroke: 80,
            lineStyle: 'round'
        });
        brusher.init();
    }

    switchLocale(locale: string, event?: MouseEvent): void {
        if (event) {
            event.preventDefault();
        }

        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        });

        this.httpClient.post<any>(`/switch_locale/${locale}`, {}, {headers: headers})
            .subscribe((res) => {
                if (res['success']) {
                    this.pageReload();
                }
            });
    }

    pageReload(): void {
        window.location.reload();
    }

}
