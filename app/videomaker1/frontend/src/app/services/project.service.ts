import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';

import {DataService} from './data-service.abstract';
import {Project} from '../models/project.model';
import {Media} from '../models/media.model';

@Injectable({
    providedIn: 'root'
})
export class ProjectService extends DataService<Project> {

    constructor(http: HttpClient) {
        super(http);
        this.setRequestUrl('api/project');
    }

    saveProjectData(projectUniqueId: string, mediaItems: {options: any, items: Media[]}): Observable<any> {
        const url = this.getRequestUrl() + `/${projectUniqueId}`;
        return this.http.post<any>(url, mediaItems, {headers: this.headers}).pipe(
            catchError(this.handleError<any>())
        );
    }

    getProgress(projectUniqueId: string): Observable<any> {
        const url = `api/project_progress/${projectUniqueId}`;
        return this.http.get<any>(url, {headers: this.headers}).pipe(
            catchError(this.handleError<any>())
        );
    }

    getMusic(): Observable<string[]> {
        const url = 'api/library_music';
        return this.http.get<string[]>(url, {headers: this.headers}).pipe(
            catchError(this.handleError<string[]>())
        );
    }

}
