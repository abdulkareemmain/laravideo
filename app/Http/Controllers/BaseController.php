<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends AbstractController
{

    /**
     * @param $userId
     * @return string
     */
    public function getTmpUserDirPath($userId)
    {
        return $this->getParameter('app.temp_dir_path') . "/{$userId}";
    }

    /**
     * @param $queryString
     * @return mixed
     */
    public function getQueryOptions($queryString)
    {
        parse_str($queryString, $options);
        return $options;
    }

    /**
     * @param $message
     * @param int $status
     * @return JsonResponse
     */
    public function setError($message, $status = Response::HTTP_UNPROCESSABLE_ENTITY)
    {
        /** @var TranslatorInterface $translator */
        $translator = $this->get('translator');
        $response = new JsonResponse(["error" => $translator->trans($message)]);
        $response = $response->setStatusCode($status);
        return $response;
    }
}
