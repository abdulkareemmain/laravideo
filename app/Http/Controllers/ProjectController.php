<?php

namespace App\Http\Controllers;

use Andchir\VideoProcessing;
use App\Media;
use App\Project;
use Doctrine\ORM\EntityManagerInterface;
use File;
use Response;
use Illuminate\Http\Request;
use Symfony\Component\BrowserKit\Response as BrowserKitResponse;
use Session;
use Cache;
class ProjectController extends BaseController
{
    private $loader;
    public function __construct()
    {
        
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $a=uniqid();
        $project=Project::create(['unique_id'=>$a,'status'=>1,'is_public'=>1]);
        $i=0;
        foreach($request->files as $key=>$image){
            $b=uniqid();
            $nameonly=preg_replace('/\..+$/', '', $image->getClientOriginalName());
            $filename=$nameonly.'_'.$b.'.'.$image->getClientOriginalExtension();
            $image->move(app_path('videomaker/public/').'userfiles/projects/'.$a,$filename);
            Media::create([
                'file_name'=>$filename,
                'title'=>$nameonly,
                'project_id'=>$project->id,
                'type'=>'image',
                'size'=>4000,
                'order_index'=>$i,

            ]);
                $i++;
        }
        return response()->json([
            'projectUniqueId' => $a,
            'errors' => false,
            'success' => true
        ]);
    }

    public function libraryMusicAction()
    {
        $libraryDirPath = public_path('library/music');
        $files = scandir($libraryDirPath);
        $files = array_filter($files, function($fileName) {
            return !in_array($fileName, ['.', '..']);
        });

        $files = array_merge($files);
        natsort($files);

        return response()->json($files);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project_id=Media::where('id',$request->items[0]['id'])->first()->project_id;
        $options=json_encode($request->options);
        Project::where('id',$project_id)->update(['options'=>$options]);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return view('steptwo');
        $project_id=\DB::table('project')->where('unique_id',$id)->first()->id;
        $mediaList=Media::where('project_id',$project_id)->get();
        $items = [];
        foreach ($mediaList as $key=>$media) {
            $items[$key]=[
                "id" => $media->id,
                "title" => "0054633_ary-jewellers-22-kt-gold-ring_450",
                "isActive" => true,
                "size" => 4000,
                "fileName" => $media->file_name,
                "textTitle" => null,
                "orderIndex" => 0,
                "options" => [],
                "motionOptions" => [],
            ];
            // $items[] = $media->toArray();
        }
        // dd($items);

        return response()->json([
            'status' => 'new',
            'options' => [],
            'musicFileUrl' => '',
            'items' => $items,
            'movieUrl' => ''
        ]);
    }

    public function image($project,$image){

        $file = File::get(app_path('videomaker/public/').'userfiles/projects/'.$project.'/'.$image);
        $response = Response::make($file, 200);
        $response->header('Content-Type', 'application/pdf');
        return $response;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function project_progress($uniqueId, Request $request){
        // $repository = $em->getRepository(Project::class);
        /** @var Session $session */
        // $session = $this->get('session');
        dd('hh');

        $output = [];
        $content = $request->getContent();
        $content = json_decode($content, true);
        if (empty($content['items'])) {
            $content['items'] = [];
        }

        /** @var Project $project */
        $project = Project::where('unique_id',$uniqueId)->first();
        if (!$project) {
            return $this->setError('Project not found.');
        }

        if ($project->getStatus() == Project::STATUS_COMPLETED) {
            $output['percent'] = 100;
        } else if ($project->getStatus() == Project::STATUS_PENDING) {
            $output['queue_number'] = $this->getQueueNumber($project->getId());
        }
        else if ($project->getStatus() == Project::STATUS_PROCESSING) {

            $progressLogPath = $project->getOptionValue('progressLogPath');
            if (file_exists($progressLogPath)) {

                $videoProcessing = new VideoProcessing([
                    'melt_path' => $this->getParameter('app.melt_path'),
                    'tmp_dir_path' => realpath($this->getParameter('app.temp_dir_path')),
                    'session_start' => false
                ]);
                $output['percent'] = $videoProcessing->getRenderingPercent($progressLogPath);
                if ($output['percent'] === null) {
                    $output['percent'] = 0;
                }
            } else {
                $output['percent'] = 100;
            }
            if ($output['percent'] >= 99) {
                $this->kickQueue();
            }
        }
        // $output['percent'] = 30;

        return response()->json($output);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * @Route("/api/project/{uniqueId}", name="project_get", methods={"get"})
     * @param string $uniqueId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function indexAction($uniqueId)
    {
        // $repository = $em->getRepository(Project::class);
        // $mediaRepository = $em->getRepository(Media::class);

        /** @var Project $project */
        $project = Project::where('unique_id',$uniqueId)->first();


        if (!$project) {
            return $this->setError('Project not found.');
        }

        $musicFileUrl = '';
        $uploadDirPath = $this->getUploadDirPath();
        $projectDirPath = $uploadDirPath . DIRECTORY_SEPARATOR . $project->getUniqueId();
        // $mediaList = $mediaRepository->findProjectMedia($project->getId());
        $project_id=\DB::table('project')->where('unique_id',$uniqueId)->first()->id;
        $mediaList=Media::where('project_id',$project_id)->get();
        $items = [];
        /** @var Media $media */
        foreach ($mediaList as $media) {
            if ($media->getType() == Media::TYPE_IMAGE) {
                $items[] = $media->toArray();
            } else if ($media->getType() == Media::TYPE_AUDIO) {
                $media->setFileTargetDir($projectDirPath);
                $musicFileUrl = '/userfiles/projects' . str_replace($uploadDirPath, '', $media->getFilePath());
            }
        }
        $movieUrl = '';
        if ($project->getStatus() === Project::STATUS_COMPLETED) {
            $movieMedia = $project->getMovie();
            if ($movieMedia) {
                $movieMedia->setFileTargetDir($projectDirPath);
                $movieUrl = '/userfiles/projects' . str_replace($uploadDirPath, '', $movieMedia->getFilePath());
            }
        }

        return $this->json([
            'status' => $project->getStatus(),
            'options' => $project->getOptions(),
            'musicFileUrl' => $musicFileUrl,
            'items' => $items,
            'movieUrl' => $movieUrl
        ]);
    }

    /**
     * @Route("/api/project/{uniqueId}", name="project_update", methods={"post"})
     * @param string $uniqueId
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function updateAction($uniqueId, Request $request) {
        // $repository = $em->getRepository(Project::class);
        // $mediaRepository = $em->getRepository(Media::class);

        $content = $request->getContent();
        $content = json_decode($content, true);
        if (empty($content['items'])) {
            $content['items'] = [];
        }
        if (empty($content['options'])) {
            $content['options'] = [];
        }
        /** @var Project $project */
        $project = Project::where('unique_id',$uniqueId)->first();

        if (!$project) {
            return $this->setError('Project not found.');
        }

        $project_id=\DB::table('project')->where('unique_id',$uniqueId)->first()->id;
        $mediaList=Media::where('project_id',$project_id)->get();
        /** @var Media $media */
        foreach ($mediaList as $media) {
            $orderIndex = array_search($media->id, array_column($content['items'], 'id'));
            if ($orderIndex === false) {
                $orderIndex = 0;
            } else {
                // dd($content);
                // $options = $content['items'][$orderIndex]['options'] ?? [];
                // $motionOptions = $content['items'][$orderIndex]['motionOptions'] ?? [];
                // $media
                //     ->setIsActive($content['items'][$orderIndex]['isActive'])
                //     ->setTextTitle($content['items'][$orderIndex]['textTitle'])
                //     ->setOptions($options)
                //     ->setMotionOptions($motionOptions);
            }
            // $media->setOrderIndex($orderIndex);
            // $em->flush();
        }
        $activeMedia = Media::where('project_id',$project->id)->get();
        if (count($activeMedia) == 0) {
            return $this->setError('You have no active photos in the project.');
        }
        else {

            $project
            ->setOptions($content['options'])
            ->setStatus(Project::STATUS_PENDING);

            $project->update([
                'options'=>$content['options'],
                'status'=>'pending'
            ]);
            // dd($project);
            // $em->flush();
            // dd('hh');
            exec("php /var/www/html/app/videomaker/bin/console app:queue");
            // var_dump($output);
            // $this->kickQueue();
        }

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * @Route("/api/project/{uniqueId}/upload", name="project_upload_file", methods={"post"})
     * @param string $uniqueId
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function uploadFileAction($uniqueId, Request $request, ValidatorInterface $validator, EntityManagerInterface $em) {
        $repository = $em->getRepository(Project::class);
        $mediaRepository = $em->getRepository(Media::class);
        $uploadDirPath = $this->getUploadDirPath();

        /** @var Project $project */
        $project = $repository->findOneBy([
            'uniqueId' => $uniqueId
        ]);

        if (!$project) {
            return $this->setError('Project not found.');
        }

        $type = $request->get('type', 'music');

        if (!$request->files->get('file')) {
            return $this->setError('File is empty.');
        }

        $projectDirPath = $uploadDirPath . DIRECTORY_SEPARATOR . $uniqueId;

        if (!is_dir($projectDirPath)) {
            mkdir($projectDirPath);
            chmod($projectDirPath, 0775);
        }

        $errorsArr = [];
        $musicId = 0;
        $oldMusicId = $project->getOptionValue('musicId');

        /** @var UploadedFile $file */
        $file = $request->files->get('file');

        $ext = strtolower($file->getClientOriginalExtension());
        if (!in_array($ext, ['mp3'])) {
            return $this->setError('Invalid file type. You must upload an audio file.');
        }

        $media = new Media();
        $media->setFile($file);
//        $errors = $validator->validateProperty($media, 'file', ['audio']);
//        if (count($errors) > 0) {
//            return $this->setError($errors[0]->getMessage());
//        } else {

            $media
                ->setFileTargetDir($projectDirPath)
                ->setTitle(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME))
                ->setProjectId($project->getId())
                ->setType(Media::TYPE_AUDIO)
                ->setSize($file->getSize());
            $em->persist($media);
            $em->flush();

            $musicId = $media->getId();
            $project->setOptionValue('musicId', $media->getId());
            $em->flush();

            // Delete old media
            if ($oldMusicId) {
                /** @var Media $oldMedia */
                $oldMedia = $mediaRepository->find($oldMusicId);
                if ($oldMedia) {
                    $em->remove($oldMedia);
                    $em->flush();
                }
            }

            $filePath = $media->getFilePath();
            $musicFileUrl = '/userfiles/projects' . str_replace($uploadDirPath, '', $filePath);
        //}

        return $this->json([
            'errors' => $errorsArr,
            'musicFileUrl' => $musicFileUrl,
            'musicId' => $musicId,
            'success' => empty($errorsArr)
        ]);
    }

    /**
     * @Route("/api/project_progress/{uniqueId}", name="project_progress", methods={"get"})
     * @param string $uniqueId
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function progressAction($uniqueId, Request $request) {
        // $repository = $em->getRepository(Project::class);
        /** @var Session $session */
        // $session = $this->get('session');
            
        exec("php /var/www/html/app/videomaker/bin/console app:queue",$output1);
        // dd($output);
        
        $output = [];
        $content = $request->getContent();
        $content = json_decode($content, true);
        if (empty($content['items'])) {
            $content['items'] = [];
        }

        /** @var Project $project */
        $project = Project::where('unique_id',$uniqueId)->first();
        if (!$project) {
            return $this->setError('Project not found.');
        }

        if ($project->status == 'completed') {
            dd('h');
            $output['percent'] = 100;
        } else if ($project->status== 'pending') {
            $output['queue_number'] = $this->getQueueNumber($project->id);
        }
        else if ($project->getStatus() == 'processing') {

            $progressLogPath = $project->getOptionValue('progressLogPath');
            if (file_exists($progressLogPath)) {

                $videoProcessing = new VideoProcessing([
                    'melt_path' => $this->getParameter('app.melt_path'),
                    'tmp_dir_path' => realpath($this->getParameter('app.temp_dir_path')),
                    'session_start' => false
                ]);
                $output['percent'] = $videoProcessing->getRenderingPercent($progressLogPath);
                if ($output['percent'] === null) {
                    $output['percent'] = 0;
                }
            } else {
                $output['percent'] = 100;
            }
            if ($output['percent'] >= 99) {
                // $this->kickQueue();
            }
        }
        
        return response()->json($output);
    }

    /**
     * @Route("/project_download/{uniqueId}", name="project_download", methods={"get"})
     * @param string $uniqueId
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function downloadAction($uniqueId, Request $request, EntityManagerInterface $em) {
        $repository = $em->getRepository(Project::class);

        /** @var Project $project */
        $project = $repository->findOneBy([
            'uniqueId' => $uniqueId,
            'status' => Project::STATUS_COMPLETED
        ]);

        if (!$project) {
            return $this->setError('Project not found.');
        }

        $movieMedia = $project->getMovie();
        if (!$movieMedia) {
            return $this->setError('Video file not found.');
        }

        $uploadDirPath = $this->getUploadDirPath();
        $projectDirPath = $uploadDirPath . DIRECTORY_SEPARATOR . $project->getUniqueId();

        $movieMedia->setFileTargetDir($projectDirPath);
        $filePath = $movieMedia->getFilePath();
        if (!file_exists($filePath)) {
            return $this->setError('Video file not found.');
        }

        $movieSize = $project->getOptionValue('size') ?? 'hd';
        $ext = strtolower(pathinfo($filePath, PATHINFO_EXTENSION));
        $fileName = date('Y-m-d H:i:s') . "_{$movieSize}.{$ext}";

        return self::downloadFile($filePath, $fileName);
    }

    /**
     * @param $projectId
     * @return int
     */
    public function getQueueNumber($projectId)
    {
        $repository=Project::all();
        $pendingProjects = Project::where('status','pending')->get();

        $index = 0;
        /** @var Project $project */
        foreach ($pendingProjects as $ind => $project) {
            if ($project->id === $projectId) {
                $index = $ind;
                break;
            }
        }

        return $index + 1;
    }

    /**
     * @param null $environment
     * @return string
     * @throws \Exception
     */
    public function kickQueue($environment = null)
    {
        /** @var KernelInterface $kernel */
        $kernel = $this->get('kernel');
        if (!$environment) {
            $environment = $kernel->getEnvironment();
        }
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'app:queue'
        ]);

        $output = new BufferedOutput();
        $application->run($input, $output);

        return $output->fetch();
    }

    /**
     * Download file
     *
     * @param string $filePath
     * @param string $fileName
     * @return Response
     */
    public static function downloadFile($filePath, $fileName = ''){

        if(!file_exists($filePath)) {
            return new Response('File not found.', BrowserKitResponse::HTTP_NOT_FOUND);
        }

        $pathInfo = pathinfo($filePath);

        if(!$fileName){
            $fileName = $pathInfo['filename'];
        }
        $fileName = str_replace('%', '', $fileName);
        $fileName = preg_replace('~[\\\/]+~', '', $fileName);

        $response = new BinaryFileResponse($filePath);
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileName,
            self::toAscii($fileName)
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    /**
     * Converts UTF-8 string to ASCII.
     */
    public static function toAscii(string $s): string
    {
        static $transliterator = null;
        if ($transliterator === null && class_exists('Transliterator', false)) {
            $transliterator = \Transliterator::create('Any-Latin; Latin-ASCII');
        }
        $s = preg_replace('#[^\x09\x0A\x0D\x20-\x7E\xA0-\x{2FF}\x{370}-\x{10FFFF}]#u', '', $s);
        $s = strtr($s, '`\'"^~?', "\x01\x02\x03\x04\x05\x06");
        $s = str_replace(
            ["\u{201E}", "\u{201C}", "\u{201D}", "\u{201A}", "\u{2018}", "\u{2019}", "\u{B0}"],
            ["\x03", "\x03", "\x03", "\x02", "\x02", "\x02", "\x04"], $s
        );
        if ($transliterator !== null) {
            $s = $transliterator->transliterate($s);
        }
        if (ICONV_IMPL === 'glibc') {
            $s = str_replace(
                ["\u{BB}", "\u{AB}", "\u{2026}", "\u{2122}", "\u{A9}", "\u{AE}"],
                ['>>', '<<', '...', 'TM', '(c)', '(R)'], $s
            );
            $s = iconv('UTF-8', 'WINDOWS-1250//TRANSLIT//IGNORE', $s);
            $s = strtr($s, "\xa5\xa3\xbc\x8c\xa7\x8a\xaa\x8d\x8f\x8e\xaf\xb9\xb3\xbe\x9c\x9a\xba\x9d\x9f\x9e"
                . "\xbf\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf\xd0\xd1\xd2\xd3"
                . "\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7\xe8"
                . "\xe9\xea\xeb\xec\xed\xee\xef\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf8\xf9\xfa\xfb\xfc\xfd\xfe"
                . "\x96\xa0\x8b\x97\x9b\xa6\xad\xb7",
                'ALLSSSSTZZZallssstzzzRAAAALCCCEEEEIIDDNNOOOOxRUUUUYTsraaaalccceeeeiiddnnooooruuuuyt- <->|-.');
            $s = preg_replace('#[^\x00-\x7F]++#', '', $s);
        } else {
            $s = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $s);
        }
        $s = str_replace(['`', "'", '"', '^', '~', '?'], '', $s);
        return strtr($s, "\x01\x02\x03\x04\x05\x06", '`\'"^~?');
    }

    /**
     * @return bool|string
     */
    public function getRootPath()
    {
        $rootPath = realpath($this->getParameter('kernel.root_dir') . '/../');
        return $rootPath;
    }

    /**
     * @return string
     */
    public function getUploadDirPath()
    {
        return realpath($this->getParameter('app.project_dir_path'));
    }
    /**
     * @param $userId
     * @return string
     */
    public function getTmpUserDirPath($userId)
    {
        return $this->getParameter('app.temp_dir_path') . "/{$userId}";
    }

    /**
     * @param $queryString
     * @return mixed
     */
    public function getQueryOptions($queryString)
    {
        parse_str($queryString, $options);
        return $options;
    }

    /**
     * @param $message
     * @param int $status
     * @return JsonResponse
     */
    public function setError($message, $status = Response::HTTP_UNPROCESSABLE_ENTITY)
    {
        /** @var TranslatorInterface $translator */
        $translator = $this->get('translator');
        $response = new JsonResponse(["error" => $translator->trans($message)]);
        $response = $response->setStatusCode($status);
        return $response;
    }
}
