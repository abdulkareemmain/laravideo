<?php

namespace App\Http\Controllers;

use App\Media;
use App\Project;
use App\Service\UtilsService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Http\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DefaultController extends Controller
{
    /**
     * @Route("", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $response = $this->render('default/index.html.twig', [
            'locale' => $request->getLocale(),
            'display_language_switch' => $this->getParameter('app.display_language_switch'),
            'display_homepage_video' => $this->getParameter('app.display_homepage_video'),
            'display_homepage_features' => $this->getParameter('app.display_homepage_features'),
            'languages' => $this->getParameter('app.languages') ?: []
        ]);
        $response->setEtag(md5($response->getContent()));
        $response->setPublic();
        $response->isNotModified($request);

        return $response;
    }

    /**
     * @Route("/switch_locale/{locale}", name="switch_locale", requirements={"_locale"}, defaults={"locale": ""}, methods={"post"})
     * @param Request $request
     * @param string $locale
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function switchLocaleAction(Request $request, $locale)
    {
        if ($locale) {
            $request->getSession()->set('_locale', $locale);
            return $this->json(['success' => true]);
        }
        return $this->json([]);
    }

    /**
     * @Route("/api/media", name="media_upload")
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param UtilsService $utilsService
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function mediaUploadAction(Request $request, ValidatorInterface $validator, UtilsService $utilsService, EntityManagerInterface $em)
    {
        $files = $request->files;
        $projectUniqueId = UtilsService::generatePassword() . uniqid();
        $uploadDirPath = $this->getUploadDirPath();
        $extensionsImages = ['png', 'jpg', 'jpeg', 'gif'];

        $project = new Project();
        $project
            ->setUniqueId($projectUniqueId)
            ->setStatus(Project::STATUS_NEW)
            ->setIsPublic(false);
        $em->persist($project);
        $em->flush();

        $projectDirPath = $uploadDirPath . DIRECTORY_SEPARATOR . $projectUniqueId;

        if (!is_dir($projectDirPath)) {
            mkdir($projectDirPath);
            chmod($projectDirPath, 0777);
        }

        $photosLimit = (int) $this->getParameter('app.max_photos');
        $errorsArr = [];
        $uploaded = 0;
        /** @var UploadedFile $file */
        foreach ($files as $file) {
            if ($uploaded && $uploaded >= $photosLimit) {
                break;
            }
            $ext = strtolower($file->getClientOriginalExtension());
            if (!in_array( $ext, $extensionsImages)) {
                continue;
            }

            $media = new Media();
            $media->setFile($file);
            $errors = $validator->validateProperty($media, 'file', ['image']);
            if (count($errors) > 0) {
                /** @var FormError $error */
                foreach ($errors as $error) {
                    $errorsArr[] = $error->getMessage();
                }
            }
            else {

                $media
                    ->setFileTargetDir($projectDirPath)
                    ->setTitle(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME))
                    ->setProjectId($project->getId())
                    ->setType(Media::TYPE_IMAGE)
                    ->setSize($file->getSize());
                $em->persist($media);
                $em->flush();

                $uploaded++;
            }
        }

        $success = $uploaded > 0;
        if ($success) {
            // Sent email to admin about new project
            $siteUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
            $mailBody = $this->renderView(
                'emails/new_project.html.twig',
                [
                    'subject' => 'New project created',
                    'siteUrl' => $siteUrl,
                    'projectId' => $project->getUniqueId()
                ]
            );
            $utilsService->sendMail('New project created', $mailBody, $this->getParameter('app.admin_email'));
        }

        return $this->json([
            'projectUniqueId' => $projectUniqueId,
            'errors' => $errorsArr,
            'success' => $success
        ]);
    }

    /**
     * @Route("/api/library_music", name="library_music", methods={"get"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function libraryMusicAction()
    {
        $libraryDirPath = $this->getParameter('app.music_library_dir_path');
        $files = scandir($libraryDirPath);
        $files = array_filter($files, function($fileName) {
            return !in_array($fileName, ['.', '..']);
        });

        $files = array_merge($files);
        natsort($files);

        return $this->json($files);
    }

    /**
     * @return bool|string
     */
    public function getRootPath()
    {
        $rootPath = realpath($this->getParameter('kernel.root_dir') . '/../');
        return $rootPath;
    }

    /**
     * @return string
     */
    public function getUploadDirPath()
    {
        return realpath($this->getParameter('app.project_dir_path'));
    }

}
